CRM.$(function searchbycid_js($) {
  'use strict';

  // The UI
  var find_button = document.getElementById('searchbycid_js_find_button');
  find_button.addEventListener('click', apicall);

  // The search form action
  function apicall() {

    var inputfield = document.getElementById('searchbycid_js_value').value;
    if (!inputfield) {
      var resultbox = document.getElementById("searchbycid-results");
      resultbox.style.height = "auto";
      resultbox.style.backgroundColor = '#C9EEF0';
      resultbox.innerHTML = '<p class="searchbyid-notification-title">Note</p>Please enter a value';
      return;
    }

    CRM.api3('Contribution', 'get', { "sequential": 1, "id": inputfield, "api.Contact.get": {} }).done(function(result){
      var resultbox = document.getElementById("searchbycid-results");
      resultbox.style.height = "auto";

      if (result && result.is_error == 0 && result.count == 0) {
        resultbox.style.backgroundColor = '#C9EEF0';
        resultbox.innerHTML = '<p class="searchbyid-notification-title">Result</p>Not Found';
      } else if (result && result.is_error == 0 && result.count > 0) {

          var contact_name = 'NA';
          if (result['values'][0]['api.Contact.get'].values[0].sort_name) {
            contact_name = result['values'][0]['api.Contact.get'].values[0].sort_name;
          }

          resultbox.style.backgroundColor = 'rgba(0,0,0,0.05)';

          var o = '';

          o+= '<p class="searchbyid-notification-title">Result</p>';

          o+= '<div class="cell"><div class="searchbyid-column-title">Name </div>' + contact_name + '</div>';
          o+= '<div class="cell"><div class="searchbyid-column-title">Amount </div>' + result['values'][0].total_amount + '</div>';
          o+= '<div class="cell"><div class="searchbyid-column-title">CID </div>' + result['values'][0].contribution_id + '</div>';
          // o+= '<div class="cell"><div class="searchbyid-column-title">Type </div>' + result['values'][0].financial_type + '</div>';
          o+= '<div class="cell" style="width: 27%"><div class="searchbyid-column-title">Source </div>' + result['values'][0].contribution_source + '</div>';
          // date fun: https://stackoverflow.com/a/34015511
          o+= '<div class="cell"><div class="searchbyid-column-title">Received </div>' + result['values'][0].receive_date + '</div>';
          o+= '<div class="cell"><div class="searchbyid-column-title">Status </div>' + result['values'][0].contribution_status + '</div>';
          o+= '<div class="cell"><div class="searchbyid-column-title">Paid By </div>' + result['values'][0].payment_instrument + '</div>';
          o+= '<div class="cell"><a class="action-item crm-hover-button crm-popup" href="'+CRM.url('civicrm/contact/view/contribution', 'reset=1&id='+result['values'][0].contribution_id+'&cid='+result['values'][0].contact_id+'&action=view&context=search&selectedChild=contribute')+'">View</a></div>';

          resultbox.innerHTML = o;

      } else {
          resultbox.style.backgroundColor = '#fdc0be';
          resultbox.innerHTML = '<p class="searchbyid-notification-title">Error</p>' + result.error_message;
      }
    });
  }
});